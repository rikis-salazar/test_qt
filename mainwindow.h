#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    // A simple comment

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void get_1st_value();

private:
    Ui::MainWindow *ui;
    int first_pair;
    int second_pair;
};

#endif // MAINWINDOW_H
