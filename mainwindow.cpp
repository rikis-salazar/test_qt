#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QObject::connect(ui->button1, SIGNAL(button_pressed()), this, SLOT(compute_grade()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::get_1st_value(){
    this->first_pair = ui->spinBox->value();
}

